#+TITLE: CBSH | AwesomeWM Theme Showcase
#+DESCRIPTION: CBSH is a post installation script for Debian Sid (unstable) and Arch Linux. it installs Cabooshy's Qtile Desktop along with some programs he pertsonally uses to make a lightweight DE.
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "/var/www/cabooshy/content/header.org"
#+ATTR_HTML: :alt Cabooshys-Musings

* A Foreword
As the AwesomeWM themes are a new thing to me, currently the only themes available are CBSH Doom Vibrant / Black Hole.

The Default wallpapers are made with [[https://lonewolfonline.net/geometric-background-generator/][This Tool, Made my Tim Trott.]] if you want to add your own themes, leave an issue on the [[https://gitlab.com/cbsh/cbsh-configs/-/issues][gitlab repo]] and i'll compile a list of user submitted themes for CBSH.

** CBSH Doom Vibrant
the default theme for CBSH, originally started off as a variant of the Doom Emacs "doom-vibrant" theme with some more purple, it slowly became the main theme for the desktop, and really needs a new name.
#+BEGIN_EXPORT html
<figure>
    <img src="./images/CBSHVibrant.png" width="100%"/>
    <figcaption> The CBSH Vibrant Theme. </figcaption>
</figure>
<hr>
#+END_EXPORT

** CBSH Black Hole
an alternate theme to the above, this theme uses colours based off of the "Black Hole" Wallpaper from Ubuntu Xenial.
#+BEGIN_EXPORT html
<figure>
    <img src="./images/CBSHBlackHole.png" width="100%"/>
    <figcaption> The CBSH Black Hole Theme. </figcaption>
</figure>
<hr>
#+END_EXPORT

#+INCLUDE: "/var/www/cabooshy/content/footer.org"
