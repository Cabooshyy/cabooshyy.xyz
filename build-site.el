;;; build-site.el --- The Script to make my site -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Cameron Miller
;;
;; Author: Cameron Miller <https://gitlab.com/cabooshy>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: January 09, 2022
;; Modified: January 09, 2022
;; Version: 0.0.1
;; Keywords: org html website tools
;; Homepage: https://gitlab.com/cabooshy/build-site
;; Package-Requires: ((emacs "24.5"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This is the build script for my website, this allows me to generate my site from a single file instead of export all of my org files manually.
;;
;;; Code:
 
(provide 'build-site)
(require 'ox-publish)

;; Define the Publishing Project
(setq org-publish-project-alist
      `(( "pages"
                :recursive t
                :base-directory "./content"
                :base-extension "org"
                :publishing-directory "./public"
                :publishing-function org-html-publish-to-html
                :with-author nil
                :with-creator nil
                :with-title nil
                :with-toc nil
                :section-numbers nil
                :time-stamp-file nil
		:body-only nil)
        ("static"
                :base-directory "./content"
                :base-extension "css\\|txt\\|jpg\\|gif\\|png"
                :recursive t
                :publishing-directory "./public"
                :publishing-function org-publish-attachment)
        ("cabooshys-musings" :components ("pages" "static"))))

(setq org-html-validation-link nil
      org-html-head-include-scripts nil
      org-html-head-include-default-style nil
      org-html-head "<link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.css\" />"
      org-html-head-extra "<link rel=\"stylesheet\" href=\"/css/style.css\" />")

;; Generate the Site Output
(org-publish-all t)

(message "Build Completed")

;;; build-site.el ends here
 
